﻿using System;
using NUnit.Framework;

namespace challenge04.test
{
    public class TestChallenge4
    {
        Challenge4 challenge4;

        [SetUp]
        public void Setup() => challenge4 = new Challenge4();

        [Test]
        public void Test_SortVouchers_nothing_comes_from_nothing() => RunTest(string.Empty, string.Empty);

        [Test]
        public void Test_SortVouchers_nulls_throw_an_exception() => Assert.Throws<NullReferenceException>(delegate { challenge4.sortVouchers(null); });

        [Test]
        public void Test_SortVouchers_ignores_vouchers_with_fewer_than_three_colons() => RunTest("Hello to Jason Issacs,a:b,c:d:e,c:d:e:f", "c:d:e,c:d:e");

        [Test]
        public void Test_SortVouchers_Available_Before_Redeemed() => RunTest("endDate:Redeemed:id,endDate:Available:id", "endDate:Available:id,endDate:Redeemed:id");

        [Test]
        public void Test_SortVouchers_Available_Before_Expired() => RunTest("endDate:Expired:id,endDate:Available:id", "endDate:Available:id,endDate:Expired:id");

        [Test]
        public void Test_SortVouchers_Activated_Before_Redeemed() => RunTest("endDate:Redeemed:id,endDate:Activated:id", "endDate:Activated:id,endDate:Redeemed:id");

        [Test]
        public void Test_SortVouchers_Activated_Before_Expired() => RunTest("endDate:Expired:id,endDate:Activated:id", "endDate:Activated:id,endDate:Expired:id");

        [Test]
        public void Test_SortVouchers_Activated_And_Available_Sorted_by_End_Date_Ascending() => RunTest("190504:Activated:id,190501:Available:id,190502:Activated:id,190503:Available:id", "190501:Available:id,190502:Activated:id,190503:Available:id,190504:Activated:id");

        [Test]
        public void Test_SortVouchers_Expired_And_Redeemed_Sorted_by_End_Date_Descending() => RunTest("190504:Expired:id,190501:Redeemed:id,190502:Expired:id,190503:Redeemed:id", "190504:Expired:id,190503:Redeemed:id,190502:Expired:id,190501:Redeemed:id");

        [Test]
        public void Test_SortVouchers_Same_Date_Activated_Before_Available() => RunTest("190504:Available:id1,190504:Activated:id2", "190504:Activated:id2,190504:Available:id1");

        [Test]
        public void Test_SortVouchers_Same_Date_Activated_Before_Expired() => RunTest("190504:Expired:id1,190504:Redeemed:id2", "190504:Redeemed:id2,190504:Expired:id1");

        [Test]
        public void Test_SortVouchers_Available_Same_Date_Sorted_By_ID() => RunTest("190504:Available:id3,190504:Available:id1,190504:Available:id2", "190504:Available:id1,190504:Available:id2,190504:Available:id3");

        [Test]
        public void Test_SortVouchers_Activated_Same_Date_Sorted_By_ID() => RunTest("190504:Activated:id3,190504:Activated:id1,190504:Activated:id2", "190504:Activated:id1,190504:Activated:id2,190504:Activated:id3");

        [Test]
        public void Test_SortVouchers_Redeemed_Same_Date_Sorted_By_ID() => RunTest("190504:Redeemed:id3,190504:Redeemed:id1,190504:Redeemed:id2", "190504:Redeemed:id1,190504:Redeemed:id2,190504:Redeemed:id3");

        [Test]
        public void Test_SortVouchers_Expired_Same_Date_Sorted_By_ID() => RunTest("190504:Expired:id3,190504:Expired:id1,190504:Expired:id2", "190504:Expired:id1,190504:Expired:id2,190504:Expired:id3");

        [Test]
        public void Test_SortVouchers_Unexpected_Status_Same_Date_Sorted_By_ID() => RunTest("190504:Unexpected:id3,190504:Unexpected:id1,190504:Unexpected:id2", "190504:Unexpected:id1,190504:Unexpected:id2,190504:Unexpected:id3");

        [Test]
        public void Test_SortVouchers_Sample_From_Challenge_Sheet() => RunTest("190112:Available:aaaa,190112:Activated:bbbb,190111:Available:cccc,190110:Redeemed:dddd,190110:Expired:eeee,190111:Activated:ffff", "190111:Activated:ffff,190111:Available:cccc,190112:Activated:bbbb,190112:Available:aaaa,190110:Redeemed:dddd,190110:Expired:eeee");

        private void RunTest(string input, string expectedOutput)
        {
            var actualOutput = (challenge4.sortVouchers(input));
            Assert.That(actualOutput == expectedOutput, "Expected: " + expectedOutput + " Actual: " + actualOutput);
        }
    }

    public class TestVoucher
    {
        //Test removed.  Voucher Class is not public so not accessable. 
        
        //public void Test_Voucher_CompateTo_Instance_Greater_Than_Null() => Assert.That((new Voucher("a:b:c")).CompareTo(null) == 1);
    }
}