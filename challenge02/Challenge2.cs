using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace challenge02
{
    public class Challenge2
    {
        public string FixPriceLabel(string inputPriceLabel)
        {
            return GetNewPriceLabel(GetCleanPrices(GetAllPricesFromLabel(inputPriceLabel)));
        }

        private static string[] GetAllPricesFromLabel(string inputPriceLabel)
        {
            return Regex.Replace(inputPriceLabel, "[^\\d.,+-]", string.Empty).Split(',');
        }

        
        private static List<string> GetCleanPrices(string[] prices)
        {
            Array.Reverse(prices);

            var correctedPrices = new List<string>();
            var lastPriceValue = decimal.MinValue;
            
            foreach (var price in prices)
            {
                var isNumeric = decimal.TryParse(price, out var priceValue);
                if (isNumeric && priceValue > lastPriceValue)
                {
                    correctedPrices.Add(price);
                    lastPriceValue = priceValue;
                }
            }

            return correctedPrices;
        }
        
        private static string GetNewPriceLabel(List<string> fixedPrices)
        {
            var fixedPriceLabel = string.Empty;
            switch (fixedPrices.Count)
            {
                case 0:
                    break;

                case 1:
                    fixedPriceLabel = "Now £" + fixedPrices[0];
                    break;

                default:
                    fixedPriceLabel = "Was £" + fixedPrices[fixedPrices.Count - 1];
                    for (var i = fixedPrices.Count - 2; i > 0; i--)
                    {
                        fixedPriceLabel += ", then £" + fixedPrices[i];
                    }
                    fixedPriceLabel += ", now £" + fixedPrices[0];
                    break;
            }

            return fixedPriceLabel;
        }
    }
}
