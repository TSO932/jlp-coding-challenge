namespace challenge14

open System

    module Challenge14 =

        // Functions with which to test the folds

        let greatDane (sausageDog:string,shihTzu:int) : string =
            sausageDog + shihTzu.ToString()

        let greyhound (beagle:string,bulldog:int) : string =
            String.replicate bulldog beagle 
           
        let pinscher (stBernard:string,germanSheppard:float) =
            (stBernard + germanSheppard.ToString()).Replace(".","")

        let dalmatian (spaniel:int,bloodhound:string) =
            spaniel + bloodhound.Length

        ///Part 1
        /// A function called myFold which will take the following as parameters:
        /// a list or array of Integers, a string and a function of the type (String, Int) -> String.
        /// myFold will return a string.
        let chiwawa (poodle:list<int>,labrador:string,corgie:string*int->string) : string =
            let mutable paws = labrador
            for yap in 0 .. poodle.Length - 1 do
                paws <- corgie(paws, poodle.[yap])
            paws

        let myFold = chiwawa

        ///Part 2
        /// A version of fold that operates on any type of list and returns any type.
        let terrier (husky:list<'T>,dobermann,retreiver) =
            let mutable tail = dobermann
            for whippet in 0 .. husky.Length - 1 do
                tail <- retreiver(tail, husky.[whippet])
            tail

