using NUnit.Framework;

namespace challenge02.test
{
    [TestFixture]
    public class TestChallenge2
    {
        [Test]
        public void Test_Filter_Out_Price_Rise() => RunTest("Was £10, then £8, then £11, now £6", "Was £11, now £6");
        [Test]
        public void Test_Repeat_Duplicate_With_Different_Format() => RunTest("Was £18, then £17, then £18.00, now £11.50", "Was £18.00, now £11.50");
        [Test]
        public void Test_Filter_Out_Duplicate_Prices() => RunTest("Was £10, then £8, then £8, now £6", "Was £10, then £8, now £6");
        [Test]
        public void Test_Filter_Out_Prices_Lower_Than_Current_Price() => RunTest("Was £10, then £6, then £4, now £8", "Was £10, now £8");
        [Test]
        public void Test_Decimal_Formats_Preserved() => RunTest("£99.50 , £99", "Was £99.50, now £99");
        [Test]
        public void Test_Extra_Comma_After_Last_Price_Ignored() => RunTest("10, 4, 8 , err?", "Was £10, now £8");
        [Test]
        public void Test_No_Input_Numbers_Return_Empty_String() => RunTest("The quick brown fox jumped over the lazy dog. Ignores Text. Huey, Duey and Louie..", string.Empty);
        [Test]
        public void Test_Empty_Input_String_Returns_Empty_String() => RunTest(string.Empty, string.Empty);
        [Test]
        public void Test_Zero_Is_Valid() => RunTest("0", "Now £0");
        [Test]
        public void Test_Signed_Integers_Are_Valid() => RunTest("30, +£20 , £+10 , £-10 , -£20", "Was £30, then £+20, then £+10, then £-10, now £-20");
        [Test]
        public void Test_Signed_Decimals_Are_Valid() => RunTest("30.00, +£20.00 , £+10.00 , £-10.00 , -£20.00", "Was £30.00, then £+20.00, then £+10.00, then £-10.00, now £-20.00");

        [Test]
        public void Test_ArgumentNullException_Thrown() => Assert.Throws<System.ArgumentNullException>(delegate { (new Challenge2()).FixPriceLabel(null); });

        private void RunTest(string input, string expectedOutput)
        {
            var actualOutput = (new Challenge2()).FixPriceLabel(input);
            Assert.That(actualOutput == expectedOutput, "Expected: " + expectedOutput + " Actual: " + actualOutput);
        }

    }
}
