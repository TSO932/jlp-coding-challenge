﻿namespace challenge34

open System
open System.Collections.Generic

    module Challenge34 =
        let rec factorial(input:int64) =
            if input > 1L then
                input * factorial (input - 1L)
            elif input = 1L then
                input
            elif input = 0L then
                1L
            else 
                invalidArg "The factorials of negative integers cannot be computed." (string input)


        let removeVowels(input:string) =
            let isConsonant(letter:char) =
                match letter with
                | 'a' | 'e' | 'i' | 'o' | 'u' | 'A' | 'E' | 'I' | 'O' | 'U' -> false
                | _ -> true

            input |> Array.ofSeq |> Array.filter isConsonant |> String
                

        type Memoize<'K, 'V when 'K : equality>(functionToMemoize) =

            let memory = new Dictionary<'K, 'V>()

            member _.Get(input) =              
                match memory.TryGetValue (input) with
                | true, oldResult -> oldResult
                | _ ->
                    let newResult = functionToMemoize(input)
                    memory.Add(input, newResult)
                    newResult
