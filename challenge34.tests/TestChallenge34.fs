module challenge34.tests

open System
open NUnit.Framework
open challenge34

[<Test>]
let factorial_of_0_is_1 () =
    Assert.AreEqual(1L, Challenge34.factorial(0L))

[<Test>]
let factorial_of_1_is_1 () =
    Assert.AreEqual(1L, Challenge34.factorial(1L))

[<Test>]
let factorial_of_10_is_3628800 () =
    Assert.AreEqual(3628800L, Challenge34.factorial(10L))

[<Test>]
let factorial_of_14_is_87178291200 () =
    Assert.AreEqual(87178291200L, Challenge34.factorial(14L))

[<Test>]
let factorial_of_minus1_throws_an_exception () =
    Assert.Throws<ArgumentException> (fun() -> Challenge34.factorial(-1L)|> ignore) |> ignore

[<Test>]
let removeVowels_returns_only_consonants_and_other_characters () =
    Assert.AreEqual("Th qck brwn fx jmps vr th lzy dg. BCDFGHJKLMNPQRSTVWXYZ0123456789*", Challenge34.removeVowels("The quick brown fox jumps over the lazy dog. ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*"))

[<Test>]
let functionWithCache_factorial_once () =
    Assert.AreEqual(87178291200L, Challenge34.Memoize(Challenge34.factorial).Get(14L))

[<Test>]
let functionWithCache_factorial_twice () =
    let functionWithCache = Challenge34.Memoize(Challenge34.factorial)
    functionWithCache.Get(14L) |> ignore
    Assert.AreEqual(87178291200L, functionWithCache.Get(14L))

[<Test>]
let functionWithCache_removeVowels_once () =
    Assert.AreEqual("Hll t Jsn sscs.", Challenge34.Memoize(Challenge34.removeVowels).Get("Hello to Jason Issacs."))

[<Test>]
let functionWithCache_removeVowels_twice () =
    let functionWithCache = Challenge34.Memoize(Challenge34.removeVowels)
    functionWithCache.Get("Hello to Jason Issacs.") |> ignore
    Assert.AreEqual("Hll t Jsn sscs.", functionWithCache.Get("Hello to Jason Issacs."))
