﻿namespace challenge05

    module Challenge5 =

        let getGridStatus (grid: string array) =

            let colour = if (Array.exists (fun row -> (String.exists ((=) 'R') row)) grid) then 'R' else 'Y'
            
            let fourInARow = String.replicate 4 (colour.ToString())
            
            let isGameOver = not (Array.exists (fun (row:string) -> (row.Contains ".")) grid)

            let isHorizontalWin = Array.exists (fun (row:string) -> (row.ToUpper().Contains fourInARow)) grid

            let board  : char[,] = array2D grid
            let boardHeight = board.GetLength 0
            let boardWidth = board.GetLength 1

            let lastPiecePlayed = 
                let rec go y x =
                    if   x >= boardWidth then None
                    elif y >= boardHeight then go 0 (x+1)
                    elif board.[y,x] = colour then Some (y,x)
                    else go (y+1) x
                go 0 0

            let getSliceToCheck(s:seq<int*int>) =
                s |> Seq.map (fun myPiece -> board.[fst(myPiece), snd(myPiece)]) |> Seq.toArray |> System.String

            let isVerticalWin() =
                let getFourDown (y:int, x:int) =
                    getSliceToCheck(Seq.init 4 (fun myY -> (y + myY, x))) 

                if lastPiecePlayed.IsNone then false
                elif fst(lastPiecePlayed.Value) + 3 >= boardHeight then false
                else getFourDown(lastPiecePlayed.Value).ToUpper().Contains fourInARow       

            let getStepCount(y:int, x:int) = min 3 (min x y)
            
            let isPrimaryDiagonalWin() =
                let getDiagonal (y:int, x:int) =
                    let backSteps = getStepCount(x, y)
                    let forwardSteps = getStepCount ((boardWidth - x - 1),(boardHeight - y - 1))
                    let startY = y - backSteps
                    let startX = x - backSteps
                    getSliceToCheck(Seq.init (backSteps + 1 + forwardSteps) (fun i -> (startY + i, startX + i)))

                if lastPiecePlayed.IsNone then false
                else getDiagonal(lastPiecePlayed.Value).ToUpper().Contains fourInARow

            let isSecondaryDiagonalWin() =
                let getDiagonal (y:int, x:int) =
                    let backSteps = getStepCount((boardWidth - x - 1), y)
                    let forwardSteps = getStepCount(x, (boardHeight - y - 1))
                    let startY = y - backSteps
                    let startX = x + backSteps
                    getSliceToCheck(Seq.init (backSteps + 1 + forwardSteps) (fun i -> (startY + i, startX - i)))

                if lastPiecePlayed.IsNone then false
                else getDiagonal(lastPiecePlayed.Value).ToUpper().Contains fourInARow

            let isWin = isHorizontalWin || isVerticalWin() || isPrimaryDiagonalWin() || isSecondaryDiagonalWin()

            if isWin then (if colour = 'R' then "Red" else "Yellow") + " wins"
            elif isGameOver then "Draw"
            else (if colour = 'R' then "Yellow" else "Red") + " plays next"