﻿namespace challenge13.test

open System
open NUnit.Framework
open challenge13

[<TestFixture>] 
type TestChallenge13 () =
    
    let vic171 = {
        Challenge13.name = "John Lewis Head Office" ;
        Challenge13.coords = { Challenge13.lat = 51.49646<Challenge13.degLat> ; Challenge13.long = -0.141499<Challenge13.degLong> }}
    let yFenni = {
        Challenge13.name = "Abergavenny" ;
        Challenge13.coords = { Challenge13.lat = 51.818294<Challenge13.degLat> ; Challenge13.long = -3.028245<Challenge13.degLong> }}
    let bressy = {
        Challenge13.name = "Victoria Bressenden Place";
        Challenge13.coords = { Challenge13.lat = 51.498149<Challenge13.degLat> ; Challenge13.long = -0.142601<Challenge13.degLong> }}
    let branches = Seq.append (Seq.singleton yFenni) (Seq.singleton bressy)

    let trewins = {
        Challenge13.name = "John Lewis Watford";
        Challenge13.coords = { Challenge13.lat = 51.654612<Challenge13.degLat> ; Challenge13.long = -0.392278<Challenge13.degLong> }}
    let waitrose = {
        Challenge13.name = "Little Waitrose at John Lewis Watford"; Challenge13.coords = trewins.coords }
    let hornets = Seq.append (Seq.singleton waitrose) (Seq.singleton trewins)
    let wharf = {
        Challenge13.name = "Canary Wharf";
        Challenge13.coords = { Challenge13.lat = 51.504577<Challenge13.degLat> ; Challenge13.long = -0.016226<Challenge13.degLong> }}
    let hornetsAndCanaries  = Seq.append (Seq.singleton trewins) (Seq.append (Seq.singleton wharf) (Seq.singleton waitrose))

    [<Test>]
    member this.getFlatEarthDistance__this_cow_is_far_away() =
        Assert.AreEqual(130.37094499391051<Challenge13.mile>, Challenge13.getFlatEarthDistance(vic171.coords, yFenni.coords))
    
    [<Test>]
    member this.getFlatEarthDistance__this_cow_is_small() =
        Assert.AreEqual(0.12659393460586052<Challenge13.mile>, Challenge13.getFlatEarthDistance(vic171.coords, bressy.coords))

    [<Test>]
    member this.getFlatEarthDistance__is_scalar_and_has_no_direction() =
        Assert.AreEqual(Challenge13.getFlatEarthDistance(yFenni.coords, vic171.coords), Challenge13.getFlatEarthDistance(vic171.coords, yFenni.coords))

    [<Test>]
    member this.getFlatEarthDistance__returns_zero_if_inputs_are_equal() =
        Assert.AreEqual(0, Challenge13.getFlatEarthDistance(vic171.coords, vic171.coords))

    [<Test>]
    member this.getGeodeticDistance__returns_zero_if_inputs_are_equal() =
        Assert.AreEqual(0, Challenge13.getGeodeticDistance(vic171.coords, vic171.coords))

    [<Test>]
    member this.getGeodeticDistance__returns_a_feasible_distance() =
        let distance = Challenge13.getGeodeticDistance(vic171.coords, yFenni.coords)
        Assert.That(distance > 100.0<Challenge13.mile> && distance < 150.0<Challenge13.mile>)

    [<Test>]
    member this.parser__creates_branches() =
        Assert.AreEqual(branches, Challenge13.parser("Abergavenny,,-3.028245,51.818294,Victoria Bressenden Place,,-0.142601,51.498149"))

    [<Test>]
    member this.parser__dedupes_itentical_entries() =
        Assert.AreEqual(hornetsAndCanaries, Challenge13.parser("John Lewis Watford,WD17 2TW,-0.392278,51.654612,Canary Wharf,E14 5EW,-0.016226,51.504577,Little Waitrose at John Lewis Watford,WD17 2TW,-0.392278,51.654612,Canary Wharf,E14 5EW,-0.016226,51.504577"))

    [<Test>]
    member this.findNearestBranch__returns_nearest_branch() =
        Assert.AreEqual(bressy , Challenge13.findNearestBranch(branches, vic171.coords, Challenge13.getFlatEarthDistance).branch)

    [<Test>]
    member this.findNearestBranch__returns_distance() = 
        Assert.AreEqual(
            0.12659393460586052<Challenge13.mile>,
            Challenge13.findNearestBranch(branches, vic171.coords, Challenge13.getFlatEarthDistance).distance)

    [<Test>]
    member this.findNearestBranch__handles_equidistant_branches() =
        Assert.AreEqual(trewins , Challenge13.findNearestBranch(hornets, vic171.coords, Challenge13.getFlatEarthDistance).branch)

    [<Test>]
    member this.getNextArrivalDateTime__zero_distance_starts_with_20_minute_wait() =
        Assert.AreEqual(DateTime.Parse "2019-09-30T11:32:13.000",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") 0.0<Challenge13.mile>)

    [<Test>]
    member this.getNextArrivalDateTime__tiny_distance_adds_a_microsecond() =
        Assert.AreEqual(DateTime.Parse "2019-09-30T11:32:13.001",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") 0.000008333333333333333<Challenge13.mile>)

    [<Test>]
    member this.getNextArrivalDateTime__0pt00833_miles_adds_a_second() =
        Assert.AreEqual(DateTime.Parse "2019-09-30T11:32:14.000",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") 0.008333333333333333<Challenge13.mile>)

    [<Test>]
    member this.getNextArrivalDateTime__half_a_mile_adds_a_minute() =
        Assert.AreEqual(DateTime.Parse "2019-09-30T11:33:13.000",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") 0.5<Challenge13.mile>)

    [<Test>]
    member this.getNextArrivalDateTime__30_miles_adds_an_hour() =
        Assert.AreEqual(DateTime.Parse "2019-09-30T12:32:13.000",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") 30.0<Challenge13.mile>)

    [<Test>]
    member this.getNextArrivalDateTime__exception_thrown_if_journey_cannot_be_completed_in_daytime() =
        Assert.Throws<InvalidOperationException>
          (fun () -> Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") 720.0<Challenge13.mile> |> ignore)
        |> ignore
    
    [<Test>]
    member this.getNextArrivalDateTime__negative_distances_turn_back_time() =
        Assert.AreEqual(DateTime.Parse "2019-09-30T08:36:13.000",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T11:12:13.000") -88.0<Challenge13.mile>)

    [<Test>]
    member this.getNextArrivalDateTime__does_not_arrive_after_6pm() =
        Assert.AreEqual(DateTime.Parse "2019-10-01T08:10:00.000",
            Challenge13.getNextArrivalDateTime (DateTime.Parse "2019-09-30T17:35:13.000") 5.0<Challenge13.mile>)

    [<Test>]
    member this.getItinerary_handles_two_entries() =
        let itinerary = Challenge13.getItinerary (branches, DateTime.Parse "2019-10-02T08:00:00.000", Challenge13.getFlatEarthDistance)
        Assert.AreEqual(2,Seq.length itinerary)
        Assert.AreEqual("Victoria Bressenden Place", (Seq.head itinerary).branchDistance.branch.name)
        Assert.AreEqual(DateTime.Parse "2019-10-02T08:20:15.1912721", (Seq.head itinerary).arrivalDateTime)
        Assert.AreEqual("Abergavenny", (Seq.last itinerary).branchDistance.branch.name)
        Assert.AreEqual(DateTime.Parse "2019-10-02T13:00:51.5224638", (Seq.last itinerary).arrivalDateTime)

    [<Test>]
    member this.getItinerary_handles_empty_input() =
        Assert.AreEqual(0, Challenge13.getItinerary (Seq.empty<Challenge13.Branch>, DateTime.Parse "2019-10-02T08:00:00.000", Challenge13.getFlatEarthDistance)|> Seq.length)

    [<Test>]
    member this.formatLine() =
        Assert.AreEqual("Branch Name: Abergavenny\tArrival Date & Time: 05/10/2019 09:08:07\tDistance (miles): 12.300000",
            Challenge13.formatLine({ Challenge13.branchDistance = { Challenge13.branch = yFenni ; Challenge13.distance = 12.3<Challenge13.mile> } ;
                Challenge13.arrivalDateTime = DateTime.Parse "2019-10-05T09:08:07.001" }))

