﻿namespace challenge13

open System
open System.IO
open Geodesy

    module Challenge13 =
        [<Measure>] type degLat
        [<Measure>] type degLong
        [<Measure>] type mile
        type Coords = { lat:float<degLat> ; long:float<degLong> }        
        type Branch = { name:string ; coords:Coords}
        type BranchDistance = { branch:Branch ; distance:float<mile> }
        type BranchVisit = { branchDistance:BranchDistance ; arrivalDateTime:DateTime }

        let parser(branchList:string) =
            let constructBranch(vals:string array) =
                {name = vals.[0] ; coords = { lat =  float vals.[3] * 1.0<degLat> ; long = float vals.[2] * 1.0<degLong> }}

            Seq.toArray (branchList.Split ',') |> Seq.chunkBySize 4 |> Seq.map (fun x -> constructBranch(x)) |> Seq.distinct

        let processor(filePath) = 
            parser(File.ReadAllLines(filePath).[0])

        let getFlatEarthDistance(here,there) =
            let latLen = there.lat - here.lat |> (fun  x -> x * 69.1<mile/degLat>)
            let lonLen = there.long - here.long |> (fun  x -> x * 44.5<mile/degLong>)
            latLen * latLen + lonLen * lonLen |> sqrt

        let getGeodeticDistance(here,there) =
            let getGlobCoords(coords:Coords) = GlobalCoordinates(Angle(coords.lat * 1.0</degLat>), Angle(coords.long * 1.0</degLong>))
            let geoCalc = GeodeticCalculator(Ellipsoid.WGS84).CalculateGeodeticCurve(getGlobCoords(here),getGlobCoords(there))
            geoCalc.EllipsoidalDistance  / 1609.34</mile>
        
        let findNearestBranch(branches:seq<Branch>,currentLocation:Coords, distanceStrategy) =
            branches |> Seq.map (fun b -> { branch = b ; distance = distanceStrategy(currentLocation, b.coords)}) |> Seq.minBy ( fun b -> b.distance, b.branch.name)

        let getNextArrivalDateTime (lastArrival:DateTime) distance : DateTime  =
            let travelTime = TimeSpan.FromHours(distance / 30.0<mile>)
            let arrivalIfLeaveNow = lastArrival.Add(TimeSpan.FromMinutes(20.0)) + travelTime

            if arrivalIfLeaveNow <= lastArrival.Date.AddHours(18.0) then arrivalIfLeaveNow
            else
                let arrivalIfLeaveInTheMorning = lastArrival.Date.AddHours(32.0) + travelTime
                if arrivalIfLeaveInTheMorning <= lastArrival.Date.AddHours(42.0) then arrivalIfLeaveInTheMorning 
                else raise (InvalidOperationException("Journey cannot be completed before the zombies come out."))

        let getItinerary(branches:seq<Branch>, startDateTime:DateTime, distanceStrategy) : seq<BranchVisit>=
            let mutable currentCoords = { lat = 51.49646<degLat> ; long = -0.141499<degLong> }
            let mutable currentDateTime = startDateTime
            let mutable remainingBranches = Set.ofSeq branches
            let mutable itinerary = Seq.empty<BranchVisit>
            
            while Seq.length itinerary < Seq.length branches do
                let nextBranchDistance = findNearestBranch(remainingBranches, currentCoords, distanceStrategy)
                let nextBranchVisit = { branchDistance = nextBranchDistance ; arrivalDateTime = (getNextArrivalDateTime currentDateTime nextBranchDistance.distance) }
                currentCoords <- nextBranchVisit.branchDistance.branch.coords
                currentDateTime <- nextBranchVisit.arrivalDateTime
                remainingBranches <-  remainingBranches.Remove nextBranchDistance.branch
                itinerary <- Seq.append itinerary (Seq.singleton nextBranchVisit) 

            itinerary

        let formatLine(bv:BranchVisit) =
                sprintf "Branch Name: %s\tArrival Date & Time: %s\tDistance (miles): %f"
                    bv.branchDistance.branch.name
                    (bv.arrivalDateTime.ToString  "dd/MM/yyyy HH:mm:ss")
                    (bv.branchDistance.distance * 1.0</mile>)

        let calculateRoute(filepath:string) =
            getItinerary(processor(filepath), DateTime.Now, getGeodeticDistance) |> Seq.map (fun bv -> formatLine(bv)) |> String.concat "\n"