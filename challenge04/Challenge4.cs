﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace challenge04
{
    
    public class Challenge4
    {
        public string sortVouchers(string vouchers) => new VoucherList(vouchers).ToString();
    }

    class VoucherList
    {
        const char delimiter = ',';
        List<Voucher> voucherList;

        public VoucherList(string vouchers)
        {
            voucherList = (vouchers.Split(delimiter).Select(voucher => new Voucher(voucher))).ToList();
            voucherList.Sort();
        }

        public override string ToString()
        {
            var vouchers = string.Empty;
            var isFirstVoucher = true;
            foreach (var voucher in voucherList)
            {
               if (voucher.IsValid)
               {
                vouchers += (isFirstVoucher ? string.Empty : char.ToString(delimiter)) + voucher;
                isFirstVoucher = false;
               }
            }
            return vouchers;
        }
     }

    class Voucher : IComparable<Voucher>
    {
        enum VoucherStatus
        {
            Activated,
            Available,
            Redeemed,
            Expired
        }
        
        const char delimiter = ':';
        string endDate;
        string status;
        string id;

        public Voucher(string voucher)
        {
            var voucherParts = voucher.Split(delimiter);
            endDate = voucherParts[0];
            status = voucherParts.Length > 1 ? voucherParts[1] : string.Empty;
            id = voucherParts.Length > 2 ? voucherParts[2] : string.Empty;     
        }
        public override string ToString() => endDate + delimiter + status + delimiter + id;

        public bool IsValid => id != string.Empty;

        public bool IsCurrent => status == "Available" || status == "Activated";

        public int CompareTo(Voucher other)
        {
            if (other == null) return 1;

            if (this.IsCurrent != other.IsCurrent) return other.IsCurrent.CompareTo(this.IsCurrent);

            if (this.endDate != other.endDate)
            {
                return this.IsCurrent ? this.endDate.CompareTo(other.endDate) : other.endDate.CompareTo(this.endDate); 
            }

            Enum.TryParse(this.status, out VoucherStatus thisStatus);
            Enum.TryParse(other.status, out VoucherStatus otherStatus);

            if (thisStatus != otherStatus) return thisStatus.CompareTo(otherStatus);

            return this.id.CompareTo(other.id); 
        }
    }
}