using Microsoft.FSharp.Core;
using NUnit.Framework;

namespace challenge05.test
{
    public class TestChallenge5
    {
        const string defaultResponse = "Red plays next";
        
        [Test]
        public void Test_getGridStatus_null_throws_exception() => Assert.Throws<System.ArgumentNullException>(delegate { Challenge5.getGridStatus(null); });

        [Test]
        public void Test_getGridStatus_unequal_length_strings_throw_exception() => Assert.Throws<System.ArgumentException>(delegate { Challenge5.getGridStatus(new string[] {"Boom", "Oo", "Yata-Ta-Ta"}); });

        [Test]
        public void Test_getGridStatus_empty_input_response_not_defined() => Challenge5.getGridStatus(new string[] {});  //no assert because response is not defined in spec

        [Test]
        public void Test_getGridStatus_invalid_input_response_not_defined() => Test_getGridStatus(new string[] {"Hello.","to....","Jason.","Issacs"}, defaultResponse);

        [Test]
        public void Test_getGridStatus_red_starts() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                ".......",
                ".......",
                ".......",
                "......."}, "Red plays next");
                
        [Test]
        public void Test_getGridStatus_red_vertical_win() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                ".R.....",
                ".r.....",
                ".ry....",
                ".ryyy.."}, "Red wins");

        [Test]
        public void Test_getGridStatus_red_horizontal_win() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                ".......",
                ".yy....",
                ".rrRr..",
                ".ryyy.."}, "Red wins");

        [Test]
        public void Test_getGridStatus_red_secondary_diagonal_win() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                "....r..",
                "...ry..",
                "..Ryr..",
                ".ryyyr."}, "Red wins");

        [Test]
        public void Test_getGridStatus_yellow_primary_diagonal_win() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                "...y...",
                "...ry..",
                "...ryy.",
                "...rrrY"}, "Yellow wins");

        [Test]
        public void Test_getGridStatus_yellow_horizontal_win() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                ".......",
                "r......",
                "rr.....",
                "ryyyY.."}, "Yellow wins");

        [Test]
        public void Test_getGridStatus_yellow_vertical_win() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                "..Y....",
                "..y....",
                "r.y...r",
                "r.y...r"}, "Yellow wins");

        [Test]
        public void Test_getGridStatus_red_plays_next() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                ".......",
                "...ry..",
                "...ryy.",
                "..yrrrY"}, "Red plays next");

        [Test]
        public void Test_getGridStatus_yellow_plays_next_bottom_three_columns() => Test_getGridStatus(new string[] {
                ".......",
                ".......",
                ".......",
                "...ry..",
                "..Rryy.",
                "..yrrry"}, "Yellow plays next");

        [Test]
        public void Test_getGridStatus_yellow_plays_next_above_bottom_three_columns() => Test_getGridStatus(new string[] {
                ".......",
                "..R....",
                "..r...y",
                "..r...y",
                "..y...r",
                "..r...y",
                }, "Yellow plays next");
                
        [Test]
        public void Test_getGridStatus_draw() => Test_getGridStatus(new string[] {
                "yyyrRyy",
                "rrryyrr",
                "yyyrryy",
                "rrryyrr",
                "yyyrryy",
                "rrryyrr",
                }, "Draw");

        private void Test_getGridStatus(string[] inputGrid, string expectedOutput)
        {
            var actualOutput = Challenge5.getGridStatus(inputGrid);
            Assert.That(actualOutput == expectedOutput, "Expected: " + expectedOutput + " Actual: " + actualOutput);
        }
    }
}