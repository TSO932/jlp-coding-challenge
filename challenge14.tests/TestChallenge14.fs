namespace challenge14.test

open System
open NUnit.Framework
open challenge14

[<TestFixture>] 
type TestChallenge14 () =
    

    // Tests for the functions that go into the fold
    
    [<Test>]
    member this.puppy() = Assert.AreEqual("K9" , Challenge14.greatDane("K",9))
    
    [<Test>]
    member this.pooch() = Assert.AreEqual("Grr! Grr! Grr! " , Challenge14.greyhound("Grr! ",3))

    [<Test>]
    member this.mutt() = Assert.AreEqual("Wolf 0123456789" , Challenge14.pinscher("Wolf ", 0.123456789))

    [<Test>]
    member this.guidedog() = Assert.AreEqual(11 , Challenge14.dalmatian(2,"Battersea"))

    //Part 1 Tests

    [<Test>]
    member this.woof() =
        Assert.AreEqual("Woof! Woof! Woof! Woof! Woof! Woof! " , Challenge14.chiwawa([3;2],"Woof! ",Challenge14.greyhound))

    member this.bark() =
        Assert.AreEqual("The result is 1234" , Challenge14.myFold([1;2;3;4],"The result is ", Challenge14.greatDane))


    //Part 2 Tests

    [<Test>]
    member this.bone() =
        Assert.AreEqual("The result is 1234" , Challenge14.terrier([1;2;3;4],"The result is ", Challenge14.greatDane))

    [<Test>]
    member this.ball() =
        Assert.AreEqual("The result is 12345678" , Challenge14.terrier([1.2;3.4;5.6;7.8],"The result is ", Challenge14.pinscher))

    [<Test>]
    member this.fur() =
        Assert.AreEqual(25 , Challenge14.terrier(["Argos";"Gnasher";"Lassie"],7, Challenge14.dalmatian))
