﻿using System;

namespace challenge02.terminal
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter a price label to fix:");
            Console.WriteLine((new Challenge2()).FixPriceLabel(Console.ReadLine()));
        }
    }
}
