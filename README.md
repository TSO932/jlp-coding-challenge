# jlp-coding-challenge

My solutions to the [JLP Coding Challenges](https://coding-challenges.jl-engineering.net/).

## Challenge 14

**Prerequisites**

1. VS Code (https://code.visualstudio.com/download)
2. The C# extension from the VS Code Marketplace (https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
3. .NET Core 3.0 SDK (https://dotnet.microsoft.com/download)
4. Git (https://git-scm.com/download/win)
5. It's Bring Your Dog to Work Day

**Steps in VS Code**

1. Open new, empty folder
2. Open Terminal
3. `git clone https://gitlab.com/TSO932/jlp-coding-challenge.git`
4. From the `jlp-coding-challenge` folder
2. `dotnet build`

To run unit tests
1.  From the `challenge14.test` folder
2.  `dotnet test`


## Challenge 13

**Prerequisites**

1. VS Code (https://code.visualstudio.com/download)
2. The C# extension from the VS Code Marketplace (https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
3. .NET Core 3.0 SDK (https://dotnet.microsoft.com/download)
4. Git (https://git-scm.com/download/win)
5. Geodesy Library `dotnet add package Geodesy --version 2.0.5`  (https://www.nuget.org/packages/Geodesy/)

**Steps in VS Code**

1. Open new, empty folder
2. Open Terminal
3. `git clone https://gitlab.com/TSO932/jlp-coding-challenge.git`
4. From the `jlp-coding-challenge` folder
2. `dotnet build`

To run as a console application in the terminal window
1.  From the `challenge13.teminal` folder
2.  `dotnet run`

To run unit tests
1.  From the `challenge13.test` folder
2.  `dotnet test`


## Challenge 5

**Prerequisites**

1. VS Code (https://code.visualstudio.com/download)
2. The C# extension from the VS Code Marketplace (https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
3. .NET Core 3.0 SDK (https://dotnet.microsoft.com/download)
4. Git (https://git-scm.com/download/win)

**Steps in VS Code**

1. Open new, empty folder
2. Open Terminal
3. `git clone https://gitlab.com/TSO932/jlp-coding-challenge.git`
4.  From the `challenge05.test` folder
5.  `dotnet test`

---

## Challenge 4

**Quick Check** (*no local installation required*)

1. Go to [dotnetfiddle.net](https://dotnetfiddle.net/)
2. Set the Options to
   * Language: C#
   * Project Type: Console
   * Complier: Roslyn 2.0
3. Paste the contents of file `jlp-coding-challenge\challenge04\Challenge4.cs` into the browser window.
4. After the inital `using` statements on lines 1-3, paste the contents of `jlp-coding-challenge\challenge04.terminal\Program.cs`
5. Remove one of the the duplicate  `using System;` statement
5. Run


**Prerequisites**

1. VS Code (https://code.visualstudio.com/download)
2. The C# extension from the VS Code Marketplace (https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
3. .NET Core 3.0 SDK (https://dotnet.microsoft.com/download)
4. Git (https://git-scm.com/download/win)

**Steps in VS Code**

1. Open new, empty folder
2. Open Terminal
3. `git clone https://gitlab.com/TSO932/jlp-coding-challenge.git`
4. From the `jlp-coding-challenge` folder
2. `dotnet build`

To run as a console application in the terminal window
1.  From the `challenge04.teminal` folder
2.  `dotnet run`

To run unit tests
1.  From the `challenge04.test` folder
2.  `dotnet test`


---

## Challenge 2

**Quick Check** (*no local installation required*)

1. Go to [dotnetfiddle.net](https://dotnetfiddle.net/)
2. Set the Options to
   * Language: C#
   * Project Type: Console
   * Complier: Roslyn 2.0
3. Paste the contents of file `jlp-coding-challenge\challenge02\Challenge2.cs` into the browser window.
4. After the inital `using` statements on lines 1-3, paste the contents of `jlp-coding-challenge\challenge02.terminal\Program.cs`
5. Remove one of the the duplicate  `using System;` statement
5. Run


**Prerequisites**

1. VS Code (https://code.visualstudio.com/download)
2. The C# extension from the VS Code Marketplace (https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
3. .NET Core 3.0 SDK (https://dotnet.microsoft.com/download)
4. Git (https://git-scm.com/download/win)

**Steps in VS Code**

1. Open new, empty folder
2. Open Terminal
3. `git clone https://gitlab.com/TSO932/jlp-coding-challenge.git`
4. From the `jlp-coding-challenge` folder
2. `dotnet build`

To run as a console application in the terminal window
1.  From the `challenge02.teminal` folder
2.  `dotnet run`

To run unit tests
1.  From the `challenge02.test` folder
2.  `dotnet test`