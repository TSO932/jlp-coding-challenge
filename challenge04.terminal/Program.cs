﻿using System;

namespace challenge04.terminal
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Please enter a list of vouchers to sort:");
            Console.WriteLine((new Challenge4()).sortVouchers(Console.ReadLine()));
        }
    }
}
